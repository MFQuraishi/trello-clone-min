let KEY = process.env.REACT_APP_TRELLO_API_KEY;
let TOKEN = process.env.REACT_APP_AUTH_TOKEN;

export function updateCheckItem(cardId, checkItemId, state) {
  state = state ? "complete" : "incomplete";

  let url = `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}`;
  let queryParams = `&state=${state}`;
  let meta = {
    method: "PUT",
  };

  return makeRequest(url, queryParams, meta);
}

export function deleteCheckItem(checkListId, CheckItemId) {
  let url = `https://api.trello.com/1/checklists/${checkListId}/checkItems/${CheckItemId}`;
  let meta = {
    method: "DELETE",
  };

  return makeRequest(url, "", meta);
}

export function addCheckItem(id, name) {
  let url = `https://api.trello.com/1/checklists/${id}/checkItems`;
  let queryParams = `&name=${name}`;
  let meta = {
    method: "POST",
  };

  return makeRequest(url, queryParams, meta);
}

export function deleteChecklist(id) {
  let url = `https://api.trello.com/1/checklists/${id}`;
  let meta = {
    method: "DELETE",
  };

  return makeRequest(url, "", meta);
}

export function createChecklist(id, name) {
  let url = `https://api.trello.com/1/cards/${id}/checklists`;
  let queryParams = `&name=${encodeURI(name)}`;
  let meta = {
    method: "POST",
  };
  return makeRequest(url, queryParams, meta);
}

export function getCheckLists(id) {
  let url = `https://api.trello.com/1/cards/${id}/checklists`;
  let meta = {
    method: "GET",
  };

  return makeRequest(url, "", meta);
}

function makeRequest(url, queryParams, meta) {
  url += `?key=${KEY}&token=${TOKEN}${queryParams}`;
  return fetch(url, meta)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.error(err);
    });
}
