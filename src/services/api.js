let KEY = process.env.REACT_APP_TRELLO_API_KEY;
let TOKEN = process.env.REACT_APP_AUTH_TOKEN;

export function deleteCard(id) {
  let url = `https://api.trello.com/1/cards/${id}`;
  let meta = {
    method: "DELETE",
  };

  return makeRequest(url, "", meta);
}

export function archiveList(id) {
  let url = `https://api.trello.com/1/lists/${id}/closed`;
  let queryParams = `&value=true`;
  let meta = {
    method: "PUT",
  };

  return makeRequest(url, queryParams, meta);
}

export function addBoard(name, color) {
  let url = `https://api.trello.com/1/boards/`;
  let queryParams = `&name=${name}&prefs_background=${color}`;
  let meta = {
    method: "POST",
  };

  return makeRequest(url, queryParams, meta);
}

export function addList(boardId, name) {
  let url = `https://api.trello.com/1/lists`;
  let queryParams = `&name=${encodeURI(name)}&idBoard=${boardId}`;
  let meta = {
    method: "POST",
  };

  return makeRequest(url, queryParams, meta);
}

export function addCard(id, name) {
  let url = `https://api.trello.com/1/cards`;
  let queryParams = `&idList=${id}&name=${encodeURI(name)}`;

  let meta = {
    method: "POST",
    headers: {
      Accept: "application/json",
    },
  };

  return makeRequest(url, queryParams, meta);
}

export function getBoards() {
  let url = "https://api.trello.com/1/members/me/boards";
  let meta = {
    method: "GET",
  };

  return makeRequest(url, "", meta);
}

export function getLists(id) {
  let url = `https://api.trello.com/1/boards/${id}/lists`;

  let meta = {
    method: "GET",
  };

  return makeRequest(url, "", meta);
}

export function getCards(id) {
  let url = `https://api.trello.com/1/lists/${id}/cards`;

  let meta = {
    method: "GET",
  };

  return makeRequest(url, "", meta);
}

function makeRequest(url, queryParams, meta) {
  url += `?key=${KEY}&token=${TOKEN}${queryParams}`;
  return fetch(url, meta)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.error(err);
    });
}
