import React from "react";

import { Button } from "react-bootstrap";

import "./sidePanel.css";

class SidePanel extends React.Component {
  state = {
    open: false,
    content: "<-",
  };
  render() {
    return (
      <div
        // className="side-panel"
        // style={{ minWidth: `${this.state.open ? "320px" : "50px"}` }}
        className={`side-panel ${this.state.open ? "open" : "close"}`}
      >
        <Button
          variant="warning"
          className="side-panel-toggle-button"
          onClick={this.handleClick}
        >
          {this.state.open ? "<-" : "->"}
        </Button>
      </div>
    );
  }
  handleClick = () => {
    this.setState({ open: !this.state.open });
  };
}

export default SidePanel;
