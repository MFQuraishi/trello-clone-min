import React from "react";

class AddSectionHandler extends React.Component {
  state = {
    addFormOpen: false,
    textFieldValue: "",
  };

  handleAddFromOpenClose = () => {
    if (this.state.addFormOpen) {
      this.setState({ addFormOpen: false });
    } else {
      this.setState({ addFormOpen: true });
    }
  };

  handleTextChange(e) {
    this.setState({ textFieldValue: e.target.value });
  }

  handleForm = (e) => {
    e.preventDefault();
    let text = this.state.textFieldValue;
    if (text) {
      this.props.onAdd(text);
      this.setState({ textFieldValue: "" });
    }
  };
  render() {
    return this.state.addFormOpen === true ? (
      <div className="add-card-container">
        <form onSubmit={this.handleForm}>
          <input
            type="text"
            className="w-100 list-card"
            placeholder={`${this.props.placeholder}`}
            value={this.state.textFieldValue}
            onChange={(e) => this.handleTextChange(e)}
          ></input>
          <button type="submit" className="btn btn-primary btn-sm">
            {/* add card */}
            {this.props.addButtonValue}
          </button>
          <button
            className="btn btn-sm btn-warning mx-2"
            onClick={this.handleAddFromOpenClose}
          >
            close
          </button>
        </form>
      </div>
    ) : (
      <div className="add-list-section">
        <button
          className={`btn ${this.props.btnStyle} w-100`}
          onClick={this.handleAddFromOpenClose}
        >
          {this.props.closedStateButtonText}
        </button>
      </div>
    );
  }
}

export default AddSectionHandler;
