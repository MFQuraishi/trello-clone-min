import React, { Component } from "react";
import { Navbar, Dropdown, DropdownButton, Form } from "react-bootstrap";
import trelloLogo from "./../assets/images/trelloLogo.png";

class CustomNavbar extends Component {
  render() {
    return (
      <Navbar bg="dark" className="px-4" style={{ height: this.props.height }}>
        <div
          className="d-flex justify-content-start align-items-center"
          style={{ width: "80%" }}
        >
          <Navbar.Brand href="#">
            <img
              src={trelloLogo}
              width="100"
              height="25"
              className="d-inline-block align-top"
              alt="Trello logo"
            />
          </Navbar.Brand>

          <DropdownButton
            id="dropdown-basic-button"
            title="Workspaces"
            size="sm"
            variant="dark"
          >
            <Dropdown.Item href="#/action-1">Workspaces</Dropdown.Item>
          </DropdownButton>
          <DropdownButton
            id="dropdown-basic-button"
            title="Recents"
            size="sm"
            variant="dark"
          >
            <Dropdown.Item href="#/action-1">Recents</Dropdown.Item>
          </DropdownButton>
          <DropdownButton
            id="dropdown-basic-button"
            title="Starred"
            size="sm"
            variant="dark"
          >
            <Dropdown.Item href="#/action-1">Starred</Dropdown.Item>
          </DropdownButton>
        </div>
        <div className="d-flex justify-content-end" style={{ width: "20%" }}>
          <Form.Control size="md" type="text" placeholder="Just a dummy" />
        </div>
      </Navbar>
    );
  }
}

export default CustomNavbar;
