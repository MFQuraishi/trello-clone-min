import React from "react";

class BoardsTopSection extends React.Component {
  render() {
    return (
      <div className="boards-top-section-container p-">
        <h1>Workspace</h1>
      </div>
    );
  }
}

export default BoardsTopSection;
