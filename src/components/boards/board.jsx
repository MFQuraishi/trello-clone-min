import React from "react";

import "./boardsSection.css";
import { NavLink } from "react-router-dom";
// import { Card, Button } from "react-bootstrap";

class Board extends React.Component {
  render() {
    let { id, prefs, name, onBoardClick } = this.props;
    return (
      <div className="board-container" onClick={() => onBoardClick(id)}>
        <NavLink to={`/${id}/${name}`}>
          <div
            className="board"
            style={
              prefs.backgroundImage === null
                ? { backgroundColor: `${prefs.backgroundColor}` }
                : { backgroundImage: `url(${prefs.backgroundImageScaled[3].url})` }
            }
          >
            <h3>{name}</h3>
          </div>
        </NavLink>
      </div>
    );
  }
}

export default Board;
