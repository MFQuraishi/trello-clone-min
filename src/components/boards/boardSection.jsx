import React from "react";
import { Container } from "react-bootstrap";

import "./boardsSection.css";

import BoardsTopSection from "./boardsTopSection";
import BoardsBottomSection from "./boardsBottomSection";

class BoardsSection extends React.Component {
  render() {
    return (
      <Container fluid className="p-0 overflow-auto">
        <BoardsTopSection />
        <BoardsBottomSection onBoardClick={this.props.onBoardClick} />
      </Container>
    );
  }
}

export default BoardsSection;
