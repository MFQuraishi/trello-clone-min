import React, { Component } from "react";

import { Container, Modal } from "react-bootstrap";

import Board from "./board";
import * as trelloApi from "./../../services/api.js";

// import tempData from "./tempData.json";

class BoardsBottomSection extends Component {
  state = {
    boardsData: [],
    showModal: false,
    newBoardName: "",
    newBoardColor: "blue",
  };

  async getBoards() {
    let boards = await trelloApi.getBoards();
    this.setState({ boardsData: boards });
  }

  componentDidMount() {
    this.getBoards();
  }

  handleModalShow = () => {
    this.setState({ showModal: !this.state.showModal });
  };

  handleAddBoardFormSubmit = async (e) => {
    e.preventDefault();
    let { newBoardName, newBoardColor } = this.state;
    if (newBoardName) {
      let newBoard = await trelloApi.addBoard(newBoardName, newBoardColor);
      let oldBoardsData = this.state.boardsData;
      this.setState({ boardsData: [newBoard, ...oldBoardsData], showModal: false });
    }
  };

  handleBoardFormFields = (property, e) => {
    this.setState({ [property]: e.target.value });
  };

  render() {
    return (
      <Container className="d-flex flex-wrap ">
        <Modal show={this.state.showModal} onHide={this.handleModalShow}>
          <Modal.Body>
            <form onSubmit={this.handleAddBoardFormSubmit}>
              <label htmlFor="boardName" className="mb-2 w-100">
                Board Name
              </label>
              <input
                type="text"
                id="boardName"
                placeholder="board name"
                value={this.state.newBoardName}
                onChange={(e) => this.handleBoardFormFields("newBoardName", e)}
                className="mb-1 p-1 w-100"
              ></input>
              <select
                className="form-select mb-2"
                onChange={(e) => this.handleBoardFormFields("newBoardColor", e)}
                aria-label="Default select example"
              >
                <option defaultValue="blue">blue</option>
                <option value="orange">orange</option>
                <option value="green">green</option>
                <option value="red">red</option>
                <option value="purple">purple</option>
                <option value="pink">pink</option>
                <option value="lime">lime</option>
                <option value="sky">sky</option>
                <option value="grey ">grey</option>
              </select>
              <button type="submit" className="btn btn-success btn-md w-100">
                add
              </button>
            </form>
          </Modal.Body>
        </Modal>
        <div className="board-container">
          <button
            className="create-new-board btn btn-secondary"
            onClick={this.handleModalShow}
          >
            create new board
          </button>
        </div>
        {this.state.boardsData.map((boardData) => {
          return (
            <Board
              key={boardData.id}
              name={boardData.name}
              prefs={boardData.prefs}
              id={boardData.id}
              onBoardClick={() => this.props.onBoardClick(boardData.id)}
            ></Board>
          );
        })}
      </Container>
    );
  }
}

export default BoardsBottomSection;
