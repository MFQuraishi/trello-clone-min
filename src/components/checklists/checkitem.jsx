import React from "react";

class CheckItem extends React.Component {
  render() {
    return (
      <div className="d-flex justify-content-between align-items-center my-1 border p-1">
        <div className="form-check">
          <input
            className="form-check-input"
            type="checkbox"
            value=""
            id={this.props.id}
            checked={this.props.checked}
            onChange={() => this.props.handleChange(this.props.id, !this.props.checked)}
          />
          <label className="form-check-label" for={this.props.id}>
            {this.props.name}
          </label>
        </div>
        <button
          className="btn btn-danger btn-sm"
          onClick={() => this.props.onDelete(this.props.id)}
        >
          remove
        </button>
      </div>
    );
  }
}

export default CheckItem;
