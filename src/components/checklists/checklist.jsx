import React from "react";

import { ProgressBar } from "react-bootstrap";

import * as trelloCheckListApi from "./../../services/checkListApi";
import AddSectionHandler from "../sharedComponents/addSectionhandler";
import CheckItem from "./checkitem";

class Checklist extends React.Component {
  constructor(props) {
    super();
    this.state = {
      checkItems: props.checkItems,
    };
  }

  updateProgressBar = () => {
    console.log("it ran");
    let checkItems = this.state.checkItems;
    let completed = checkItems.reduce((prevVal, newVal) => {
      if (newVal.state === "complete") {
        return prevVal + 1;
      }
      return prevVal;
    }, 0);

    return (completed / checkItems.length) * 100;
  };

  addCheckItem = async (name) => {
    let newCheckItem = await trelloCheckListApi.addCheckItem(this.props.id, name);
    if (newCheckItem.name === name) {
      this.setState({
        checkItems: [...this.state.checkItems, newCheckItem],
      });
    }
  };

  handleCheckItemDelete = async (id) => {
    let deleteResponse = await trelloCheckListApi.deleteCheckItem(this.props.id, id);
    if (typeof deleteResponse === "object") {
      let newCheckItems = this.state.checkItems.filter(
        (checkItem) => checkItem.id !== id
      );
      this.setState({ checkItems: newCheckItems });
    }
  };

  handleCheckItemChange = async (id, state) => {
    let response = await trelloCheckListApi.updateCheckItem(this.props.cardId, id, state);

    if (typeof response === "object") {
      let newCheckItems = this.state.checkItems.map((checkItem) => {
        if (checkItem.id === id) {
          if (checkItem.state === "complete") {
            checkItem.state = "incomplete";
          } else {
            checkItem.state = "complete";
          }
        }
        return checkItem;
      });

      console.log(newCheckItems);
      this.setState({ checkItems: newCheckItems });
    }
  };

  render() {
    let progress = this.updateProgressBar();
    return (
      <div className="checklist-wrapper p-2 border mx-3 mt-2">
        <div className="checklist-header d-flex justify-content-between mb-1">
          <div className="checklist-name">{this.props.name}</div>
          <button
            className="btn btn-danger btn-sm"
            onClick={() => this.props.onDelete(this.props.id)}
          >
            delete checklist
          </button>
        </div>
        <div className="checklist-container">
          <div className="my-2">
            <ProgressBar variant={progress >= 100 ? "success" : "info"} now={progress} />
          </div>
          {this.state.checkItems.map((checkItem) => {
            console.log(checkItem.name, "=", checkItem.state);
            console.log(checkItem.state === "complete" ? "true" : "false");
            return (
              <CheckItem
                name={checkItem.name}
                id={checkItem.id}
                checked={checkItem.state === "complete" ? true : false}
                handleChange={this.handleCheckItemChange}
                onDelete={this.handleCheckItemDelete}
              />
            );
          })}
        </div>
        <AddSectionHandler
          closedStateButtonText="+ Add checkitem"
          placeholder="enter new checkitem"
          btnStyle="btn-secondary"
          addButtonValue="add checklist"
          onAdd={this.addCheckItem}
        />
      </div>
    );
  }
}

export default Checklist;
