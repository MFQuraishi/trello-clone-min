import React from "react";
import Checklist from "../checklists/checklist";
import AddSectionHandler from "./../sharedComponents/addSectionhandler";

import * as trelloCheckListApi from "./../../services/checkListApi";

class CardModalContent extends React.Component {
  constructor() {
    super();
    this.state = {
      checklists: [],
    };
  }

  addCheckList = async (checklistName) => {
    let newChecklist = await trelloCheckListApi.createChecklist(
      this.props.cardId,
      checklistName
    );
    this.setState({
      checklists: [...this.state.checklists, newChecklist],
    });
  };

  handleChecklistDelete = async (id) => {
    let deleteResponse = await trelloCheckListApi.deleteChecklist(id);
    if (typeof deleteResponse === "object") {
      let newChecklists = this.state.checklists.filter(
        (checklist) => checklist.id !== id
      );
      this.setState({
        checklists: newChecklists,
      });
    }
  };

  updateChecklists = async () => {
    let checklists = await trelloCheckListApi.getCheckLists(this.props.cardId);
    this.setState({ checklists });
  };

  componentDidMount() {
    this.updateChecklists();
  }

  render() {
    return (
      <div className="modal-container ">
        <div className="modal-left-section">
          {this.state.checklists.map((checklist) => {
            return (
              <Checklist
                key={checklist.id}
                name={checklist.name}
                id={checklist.id}
                cardId={this.props.cardId}
                checkItems={checklist.checkItems}
                onDelete={this.handleChecklistDelete}
              />
            );
          })}
        </div>
        <div className="modal-right-section">
          <div className="add-checklist-container">
            <div className="border p-1 ">
              <AddSectionHandler
                closedStateButtonText="+ Add another checklist"
                placeholder="enter new checklist"
                btnStyle="btn-secondary"
                addButtonValue="add checklist"
                onAdd={this.addCheckList}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CardModalContent;
