import React from "react";

import { Modal } from "react-bootstrap";
import CardModalContent from "./cardModalContent";

class ListCard extends React.Component {
  constructor() {
    super();
    this.state = {
      showModal: false,
    };
  }

  handleModalShow = () => {
    console.log("card clicked");
    this.setState({ showModal: !this.state.showModal });
  };

  render() {
    let { name, id } = this.props.info;
    return (
      <>
        <Modal
          variant="primary"
          show={this.state.showModal}
          onHide={this.handleModalShow}
          size="lg"
        >
          <Modal.Header>
            <Modal.Title>{name}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <CardModalContent cardId={id} />
          </Modal.Body>
        </Modal>
        <div className="list-card-container">
          <div className=" btn list-card" onClick={this.handleModalShow}>
            <span>{name}</span>
          </div>
          <button
            className="btn btn-sm btn-danger card-delete-button"
            onClick={() => this.props.onDelete(id)}
          >
            delete
          </button>
        </div>
      </>
    );
  }
}

export default ListCard;
