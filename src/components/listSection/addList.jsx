import React from "react";
import AddSectionHandler from "./../sharedComponents/addSectionhandler";

class AddList extends React.Component {
  render() {
    return (
      <div className="list-wrapper">
        <div className="list-container bg-secondary rounded-2">
          <AddSectionHandler
            closedStateButtonText="+ Add another list"
            placeholder="enter new list name"
            btnStyle="btn-secondary"
            addButtonValue="add list"
            onAdd={this.props.onAddList}
          />
        </div>
      </div>
    );
  }
}

export default AddList;
