import React from "react";

import { Container } from "react-bootstrap";

import List from "./list";
import AddList from "./addList";
import * as trelloApi from "./../../services/api";

import "./listSection.css";

// import { lists } from "./../../demoData/demoListData";

class ListSection extends React.Component {
  constructor(props) {
    super();
    const boardInfo = props.match.params;
    this.boardId = boardInfo.boardId;
    this.boardName = boardInfo.boardName;

    this.state = {
      lists: [],
    };
  }

  async getLists() {
    let lists = await trelloApi.getLists(this.boardId);
    this.setState({ lists });
  }

  componentDidMount() {
    this.getLists();
  }

  handleAddList = async (name) => {
    let oldLists = this.state.lists;
    let newList = await trelloApi.addList(this.boardId, name);

    this.setState({ lists: [...oldLists, newList] });
  };

  archiveList = async (id) => {
    let archivedList = await trelloApi.archiveList(id);
    if (archivedList.closed) {
      let newLists = this.state.lists.filter((list) => list.id !== archivedList.id);
      this.setState({ lists: newLists });
    } else {
      console.err("there was problem in archiving list");
    }
  };

  render() {
    return (
      <Container fluid className="p-0 d-flex flex-column list-section">
        <div className="d-flex justify-content-center align-items-center p-1 board-name-heading">
          <h1>{this.boardName}</h1>
        </div>
        <Container fluid className="p-0 d-flex list-section">
          {this.state.lists.map((list) => {
            return <List key={list.id} info={list} onArchive={this.archiveList} />;
          })}
          <AddList onAddList={this.handleAddList} />
        </Container>
      </Container>
    );
  }
}

export default ListSection;
