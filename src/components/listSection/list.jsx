import React from "react";
import ListCard from "./listCard";
import * as trelloApi from "./../../services/api";

import AddSectionHandler from "./../sharedComponents/addSectionhandler";

class List extends React.Component {
  state = {
    cards: [],
    addCardFormOpen: false,
  };

  async getListData(id) {
    let cards = await trelloApi.getCards(id);
    this.setState({ cards });
  }

  componentDidMount() {
    this.getListData(this.props.info.id);
  }

  handleAddCard = async (cardName) => {
    let listId = this.props.info.id;
    let currentCardState = this.state.cards;
    let newCard = await trelloApi.addCard(listId, cardName);
    this.setState({ cards: [...currentCardState, newCard] });
  };

  deleteCard = async (id) => {
    console.log("delete card", id);
    let response = await trelloApi.deleteCard(id);
    if (typeof response === "object") {
      let newCards = this.state.cards.filter((card) => card.id !== id);
      this.setState({ cards: newCards });
    } else {
      console.error("there was a problem in deleting card");
    }
  };

  render() {
    let { name } = this.props.info;

    return (
      <div className="list-wrapper">
        <div className="list-container">
          <div className="list-heading-section">
            <h5>{name}</h5>
            <button
              className="btn btn-danger btn-sm list-archive-button"
              onClick={() => this.props.onArchive(this.props.info.id)}
            >
              archve list
            </button>
          </div>
          <div>
            {this.state.cards.map((card) => {
              return <ListCard key={card.id} info={card} onDelete={this.deleteCard} />;
            })}
          </div>
          <AddSectionHandler
            closedStateButtonText="+ add a card"
            placeholder="enter card name"
            btnStyle="custom-addCard-button"
            addButtonValue="add card"
            onAdd={this.handleAddCard}
          />
        </div>
      </div>
    );
  }
}

export default List;
