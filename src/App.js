import React from "react";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import CustomNavbar from "./components/navbar";
import SidePanel from "./components/sidePanel/sidePanel";
import BoardsSection from "./components/boards/boardSection";
import ListSection from "./components/listSection/listSection";

import "./app.css";

class App extends React.Component {
  state = {
    // boardId: "6167de5c17052e8a211cc878",
    // boardId: "",
  };

  handleBoardClick(id) {
    console.log(id);
    // this.setState({ boardId: id });
  }

  render() {
    const navBarHeight = "56px";
    return (
      <React.Fragment>
        <CustomNavbar height={navBarHeight} />
        <div
          className="side-panel-and-content-container"
          style={{ height: `calc(100% - ${navBarHeight})` }}
        >
          <SidePanel />

          <Router>
            <Switch>
              <Route exact path="/">
                <BoardsSection onBoardClick={this.handleBoardClick} />
              </Route>
              <Route path="/:boardId/:boardName" component={ListSection} />
              {/* boardId={this.state.boardId} */}
              {/* <ListSection temp="temp" /> */}
              {/* </Route> */}
            </Switch>
          </Router>
        </div>
      </React.Fragment>
    );
  }
}

export default App;
